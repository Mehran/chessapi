import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for,make_response
)

from flaskr.repository.keysRepo import is_valid
from flaskr.chess import convert_pgn
from redis import Redis
from rq import Queue
from flaskr.repository import gamesRepo
from flaskr.models import KeysConstants
import json

bp = Blueprint('api', __name__, url_prefix='/api')

@bp.before_request
def is_valid_key():
    key=request.headers.get("apiKey")
    if key is None:
        return "{'status' : 'false' }"
    if key == KeysConstants.ADMIN_INTERNAL_KEY:
        return
    if not is_valid(key) :
        return "{'status' : 'false' }"

@bp.route("/add",methods=['GET', 'POST'])
def addGame():

    r = Redis()
    q = Queue(connection=r)

    if request.data:

        job = q.enqueue(convert_pgn, request.data)

        return "{'status' : 'true' }"

    return "{'status' : 'false' }"

@bp.route('/finish',methods=['POST'])
def finishGame():
    js=request.data
    arr=json.loads(js)
    gamesRepo.newGame(arr["gameid"],arr["userid"],arr["verdict"],arr['pgn'],float(arr['model1res']),float(arr["model2res"]))
    return "{'status' : 'true' }"