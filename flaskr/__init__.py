import os

from flask import Flask, g, redirect,  url_for




def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:nafas eshghe@localhost/chess'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass



    from . import db
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)



    from  flaskr.admin import bp as adminbp
    import rq_dashboard
    app.config.from_object(rq_dashboard.default_settings)

    def checklogin():
        if g.user is None:
            return redirect(url_for('auth.login'))

    rq_dashboard.blueprint.before_request(checklogin)
    app.register_blueprint(rq_dashboard.blueprint, url_prefix="/jobs")

    adminbp.before_request(checklogin)
    app.register_blueprint(adminbp)

    from . import api
    app.register_blueprint(api.bp)

    return app
