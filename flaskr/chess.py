import numpy as np
import pandas as pd
import subprocess
import json
from redis import Redis
from rq import Queue
import tensorflow as tf
from tensorflow.keras.models import load_model
from flaskr.repository import gamesRepo
from flaskr.models import GameVerdicts
from flaskr.models import KeysConstants
import requests

def moves_to_fen(fens):
    fenlist = []

    for fen in fens:
        fen = fen.split(" ")[0]
        fen = fen.replace("1", "0").replace("2", "00").replace("3", "000").replace("4", "0000")
        fen = fen.replace("5", "00000").replace("6", "000000").replace("7", "0000000").replace("8", "00000000")
        fen = list(fen.replace("/", ""))
        fen = np.array(fen)
        fen = np.reshape(fen, [8, 8])
        fenlist.append(fen)

    return fenlist


def getDataTensor(df, tstart, tend):
    pgns = df['Positions']
    s = len(pgns)
    t = tend - tstart
    tensor = np.zeros((s, t, 12, 8, 8))
    for c, pgn in enumerate(pgns):
        game = getGame(pgn, tstart, tend)
        tensor[c] = game
    return tensor


def getGame(pgn, tstart, tend):
    t = tend - tstart
    game = np.zeros((t, 12, 8, 8))

    fenlist = moves_to_fen(pgn)
    while len(fenlist) < tend:
        fenlist.append(np.zeros((12, 8, 8)))
    fenlist = fenlist[tstart:tend]
    for c, fen in enumerate(fenlist):
        position = getPosition(fen)
        game[c] = position
    return game


def getPosition(fen):
    position = np.zeros((12, 8, 8))
    pieces = ["P", "R", "N", "B", "Q", "K", "p", "r", "n", "b", "q", "k"]
    for c, piece in enumerate(pieces):
        channel = (fen == piece)
        position[c] = channel
    return position



def save_database(arr):
    model1res=arr["model1"][0][0]
    model2res=arr["model1"][0][1]/(arr["model1"][0][1]+arr["model1"][0][2])
    verd=tf.argmax(arr["model1"],1)[0]
    if verd == 0:
        verdict = GameVerdicts.NO_CHEAT
    elif verd == 2:
        verdict = GameVerdicts.WHITE_CHEATER
    else:
        verdict = GameVerdicts.BLACK_CHEATER
    newarr={"gameid":arr['gameid'],'userid':arr['userid'],'verdict':verdict,'pgn':arr['pgn'],'model1res':str(model1res),'model2res':str(model2res)}

    try:
        print(requests.post("http://localhost/api/finish",json.dumps(newarr),headers={'apiKey':KeysConstants.ADMIN_INTERNAL_KEY}))
    except:
        print("exception local")
    newarr = [{"gameid": arr['gameid'], 'userid': arr['userid'], 'verdict': verdict,
              'model1res': str(model1res), 'model2res': str(model2res)}]

    try:
        print(requests.post("https://185.126.200.202:8081/api/result/submit",json.dumps(newarr)))
    except:
        print("exception local")
    return

def run_model_2(arr):

    model = load_model("chess_HoC.h5")
    res=model.predict(arr['data'])
    arr['model2']=res

    r = Redis()
    q = Queue(connection=r)

    job = q.enqueue(save_database, arr)

def run_model_1(arr):
    model = load_model("chess_last.h5")
    res = model.predict(arr['data'])
    arr['model1']=res

    r = Redis()
    q = Queue(connection=r)

    job = q.enqueue(save_database, arr)



def convert_pgn(pgn):
    arr = json.loads(pgn)
    process = subprocess.Popen(["./pgn-extract","-Wejson"],
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE)
    stdout_value = process.communicate(('[Event "FICS rated standard game"]'+'\n'
'[Site "FICS freechess.org"]'+'\n'
'[FICSGamesDBGameNo "470452366"]'+'\n'
'[White "defensiveone"]'+'\n'
'[Black "GriffySr"]'+'\n'
'[WhiteElo "2127"]'+'\n'
'[BlackElo "2278"]'+'\n'
'[WhiteRD "38.2"]'+'\n'
'[BlackRD "26.2"]'+'\n'
'[BlackIsComp "Yes"]'+'\n'
'[TimeControl "900+0"]'+'\n'
'[Date "2020.01.31"]'+'\n'
'[Time "23:36:00"]'+'\n'
'[WhiteClock "0:15:00.000"]'+'\n'
'[BlackClock "0:15:00.000"]'+'\n'
'[ECO "A07"]'+'\n'
'[PlyCount "136"]'+'\n'
'[Result "0-1"]'+'\n'+arr['pgn']).encode())[0]

    process.terminate()

    df=pd.read_json(stdout_value, lines=True)

    df = df.drop(
        ['chash', 'fhash', 'Black', 'BlackClock', 'Time', 'BlackRD', 'Date', 'ECO', 'Event', 'FICSGamesDBGameNo',
         'Round', 'Site', 'WhiteRD', 'White', 'WhiteClock'], axis=1)


    data=getDataTensor(df,30, 60)

    arr['data']=data

    r = Redis()
    q = Queue(connection=r)

    job = q.enqueue(run_model_1, arr)
