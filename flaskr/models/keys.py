def load_key(db):

    class Key(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        active = db.Column(db.Integer, nullable=False)
        key = db.Column(db.String(100), nullable=False)
    return Key