def load_game(db):

    class Game(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        gameid=db.Column(db.Integer, nullable=False)
        userid=db.Column(db.Integer, nullable=False)
        verdict=db.Column(db.Integer, nullable=False)
        pgn=db.Column(db.String(1000),  nullable=False)
        model1result=db.Column(db.Float, nullable=False)
        model2result=db.Column(db.Float, nullable=False)
    return Game