import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash
def get_db():
    db = SQLAlchemy(current_app)
    return db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

def init_db():
    from flaskr.models import load_user,load_game,load_key
    db = get_db()
    load_key(db)
    user=load_user(db)
    load_game(db)
    db.create_all()
    u=user(username='admin',password=generate_password_hash("afa@fada@#s"))
    db.session.add(u)
    db.session.commit()
    return db
@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')



def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)


