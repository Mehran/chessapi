
from flask import (
    Blueprint
)

bp = Blueprint('admin', __name__, url_prefix='/admin')

from .games import *
from .keys import *
from .users import *