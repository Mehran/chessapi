from flaskr.admin import bp
from flaskr.repository import gamesRepo
from flask import render_template
from flaskr.models import GameVerdicts

@bp.route('/games/')
@bp.route('/games/<page>')
def games(page=1):

    pages=gamesRepo.getPage(page,5)
    data=None
    total=0
    if pages :
        data=pages.items
        total=pages.pages
    return render_template("admin/games.html",games=data,total=total,page=int(page),verdicts=GameVerdicts.Verdicts)

@bp.route('/games/view/<id>')
def view(id):
    game=gamesRepo.getGamebyId(id)
    return render_template("admin/gameview.html",game=game)
