from flaskr.admin import bp
from flaskr.repository import keysRepo
from flask import render_template

@bp.route('/keys/<action>/<id>')
@bp.route('/keys/<action>')
@bp.route('/keys/')
def keys(action=None,id=None):
    pagee =1
    if action == "add":
        keysRepo.addKey()
    elif action == "delete":
        if id != None:
            keysRepo.removeKey(id)
    elif action == "active":
        if id != None:
            keysRepo.changeActive(id)
    elif action == "page" :
        if id != None:
            pagee=id

    pages=keysRepo.getPage(pagee, 4)
    data=None
    total=0
    if pages :
        data=pages.items
        total=pages.pages
    return render_template("admin/keys.html",keys=data,total=total,page=pagee)

