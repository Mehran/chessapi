from flaskr.db import get_db
from flaskr.models import load_game


def getPage(num,count):
    db = get_db()
    gameClass = load_game(db)

    return gameClass.query.order_by(gameClass.id.desc()).paginate(int(num),count)

def getGamebyId(id):
    db = get_db()
    gameClass = load_game(db)
    g=gameClass.query.filter_by(id=id).first()

    return g

def newGame(gameid,userid,verdict,pgn ,model1result ,model2result):
    db = get_db()
    gameClass = load_game(db)
    g=gameClass(gameid=gameid,userid=userid,verdict=verdict,pgn =pgn ,model1result =model1result ,model2result=model2result)

    db.session.add(g)
    db.session.commit()
