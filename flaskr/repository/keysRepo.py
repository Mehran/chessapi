from flaskr.models import load_key
from flaskr.db import get_db
from time import time
import hashlib as hash


def is_valid(key):
    db = get_db()
    keyClass = load_key(db)
    k=keyClass.query.filter_by(key=key,active=1).first()

    if k is None:
        return False
    return True

def addKey():
    db = get_db()
    keyClass = load_key(db)
    k=keyClass(key=hash.sha3_256(str(time()).encode('utf-8')).hexdigest(),active=1)
    db.session.add(k)
    db.session.commit()

def removeKey(id):
    db = get_db()
    keyClass = load_key(db)
    keyClass.query.filter_by(id=id).delete()
    db.session.commit()

def changeActive(id):
    db = get_db()
    keyClass = load_key(db)
    k=keyClass.query.filter_by(id=id).first()
    if k.active==0:
        k.active=1
    else:
        k.active=0
    db.session.commit()

def getPage(num,count):
    db = get_db()
    keyClass = load_key(db)

    return keyClass.query.order_by(keyClass.id.desc()).paginate(int(num),count)