from flaskr.models import load_user
from flaskr.db import get_db
from werkzeug.security import generate_password_hash


def loadUser(username):
    db = get_db()
    userClass = load_user(db)
    user=userClass.query.filter_by(username=username).first()
    return user

def loadUserId(id):
    db = get_db()
    userClass = load_user(db)
    user=userClass.query.filter_by(id=id).first()
    return user

def addUser(username,password):
    db = get_db()
    userClass = load_user(db)
    u = userClass(username=username, password=generate_password_hash(password))
    db.session.add(u)
    db.session.commit()
