#!/usr/bin/python
from flup.server.fcgi import WSGIServer
from flaskr import create_app
from instance.config import BIND_ADDRESS

if __name__ == '__main__':
    WSGIServer(create_app(),bindAddress=BIND_ADDRESS).run()